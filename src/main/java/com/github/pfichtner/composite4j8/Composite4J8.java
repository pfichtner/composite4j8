package com.github.pfichtner.composite4j8;

import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.function.Predicate.isEqual;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Composite4J8<T> {

	private Function<T, ? extends Iterable<T>> getChildren;
	private Predicate<T> hasChildren;

	private Composite4J8(Function<T, ? extends Iterable<T>> getChildren) {
		this.getChildren = getChildren;
	}

	public static <T> Composite4J8<T> createStreamsUsing(
			Function<T, ? extends Iterable<T>> getChildren) {
		return new Composite4J8<T>(getChildren).usingHasChildren(childrenNull(
				getChildren).negate());
	}

	public Composite4J8<T> usingHasChildren(Predicate<T> hasChildren) {
		this.hasChildren = hasChildren;
		return this;
	}

	public Stream<T> dsfStream(T node) {
		return dsfStream_internal(node).map(NodeInfo::getNode);
	}

	// TODO should we extend stream? (so we could support filter(T) instead if
	// filter(NodeInfo<T))
	@Deprecated
	// this never will be a public API
	public Stream<NodeInfo<T>> dsfStream_internal(T node) {
		return newStream(new DfsItr<T>(node, getChildren, hasChildren));
	}

	public Stream<T> bsfStream(T node) {
		return newStream(new BfsItr<T>(node, getChildren, hasChildren)).map(
				NodeInfo::getNode);
	}

	private static <T> Stream<T> newStream(Iterator<T> iterator) {
		return stream(spliteratorUnknownSize(iterator, ORDERED), false);
	}

	private static <T> Predicate<T> childrenNull(
			Function<T, ? extends Iterable<T>> getChildren) {
		return node -> isEqual(null).test(getChildren.apply(node));
	}

	/**
	 * Returns a Predicate that can test a node if the node's children are null
	 * or empty which indicates a leaf node.
	 * 
	 * @param getChildren
	 *            Function for retrieving child nodes
	 * @return Predicate that can test a node is a leaf
	 */
	public static <T> Predicate<T> isLeaf(
			Function<T, ? extends Iterable<T>> getChildren) {
		return node -> {
			Iterable<T> children = getChildren.apply(node);
			return children == null || !children.iterator().hasNext();
		};
	}

	public static class NodeInfo<T> {

		private final int level;
		private final T node;

		public NodeInfo(int level, T node) {
			this.level = level;
			this.node = node;
		}

		public int getLevel() {
			return level;
		}

		public T getNode() {
			return node;
		}

		public static Predicate<NodeInfo<?>> isLevel(int level) {
			return i -> i.getLevel() == level;
		}

	}

	private abstract static class AbstractTreeItr<T> implements
			Iterator<NodeInfo<T>> {

		private final LinkedList<NodeInfo<T>> stack;

		private final Predicate<T> hasChildren;

		private final Function<T, ? extends Iterable<T>> getChildren;

		protected AbstractTreeItr(T root, Predicate<T> hasChildren,
				Function<T, ? extends Iterable<T>> getChildren) {
			this.stack = new LinkedList<>(
					Collections.singletonList(new NodeInfo<T>(0, root)));
			this.hasChildren = hasChildren;
			this.getChildren = getChildren;
		}

		@Override
		public boolean hasNext() {
			return !this.stack.isEmpty();
		}

		protected NodeInfo<T> pop() {
			return this.stack.pop();
		}

		protected boolean hasChildren(T node) {
			return this.hasChildren.test(node);
		}

		protected void appendChildrenRight(NodeInfo<T> nodeInfo) {
			this.stack.addAll(childrenOf(nodeInfo));
		}

		protected void insertChildrenLeft(NodeInfo<T> nodeInfo) {
			this.stack.addAll(0, childrenOf(nodeInfo));
		}

		private Collection<? extends NodeInfo<T>> childrenOf(
				NodeInfo<T> nodeInfo) {
			T node = nodeInfo.getNode();
			int nextLevel = nodeInfo.getLevel() + 1;
			return stream(getChildren(node).spliterator(), false).map(
					t -> new NodeInfo<T>(nextLevel, t)).collect(toList());
		}

		private Iterable<T> getChildren(T next) {
			return this.getChildren.apply(next);
		}

	}

	private static class DfsItr<T> extends AbstractTreeItr<T> {

		public DfsItr(T root, Function<T, ? extends Iterable<T>> getChildren,
				Predicate<T> hasChildren) {
			super(root, hasChildren, getChildren);
		}

		@Override
		public NodeInfo<T> next() {
			NodeInfo<T> next = pop();
			if (hasChildren(next.getNode())) {
				insertChildrenLeft(next);
			}
			return next;
		}

	}

	private static class BfsItr<T> extends AbstractTreeItr<T> {

		public BfsItr(T root, Function<T, ? extends Iterable<T>> getChildren,
				Predicate<T> hasChildren) {
			super(root, hasChildren, getChildren);
		}

		@Override
		public NodeInfo<T> next() {
			NodeInfo<T> next = pop();
			if (hasChildren(next.getNode())) {
				appendChildrenRight(next);
			}
			return next;
		}

	}

}
