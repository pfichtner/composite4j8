package com.github.pfichtner.composite4j8.showcase;

import static com.github.pfichtner.composite4j8.Composite4J8.createStreamsUsing;
import static com.github.pfichtner.composite4j8.Composite4J8.isLeaf;
import static com.github.pfichtner.composite4j8.util.Strings.copies;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.github.pfichtner.composite4j8.Composite4J8;

public class Composite4J8Showcase {

	interface Component {
		default Collection<Component> getChildren() {
			return null;
		}
	}

	static class Composite implements Component {

		private final String name;
		private final Collection<Component> children;

		public Composite(String name, Component... children) {
			this.name = name;
			this.children = Arrays.asList(children);
		}

		@Override
		public Collection<Component> getChildren() {
			return children;
		}

		@Override
		public String toString() {
			return "Composite " + name;
		}
	}

	static class Leaf implements Component {

		private final String name;

		public Leaf(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return "Leaf " + name;
		}
	}

	private static final Composite tree = c("root",
			c("l1 A", l("l2 A"), c("l2 B", l("l3 A"), l("l3 B"))),
			c("l1 B", l("l2 C"), l("l2 D"), l("l2 E")));

	public static void main(String[] args) {
		Composite4J8<Component> streamProvider = createStreamsUsing(Component::getChildren);
		dump("dfs", streamProvider.dsfStream(tree));
		dump("bfs", streamProvider.bsfStream(tree));
		filtered();
	}

	private static void filtered() {
		Predicate<Component> isLeaf = isLeaf(Component::getChildren);
		Composite4J8<Component> streamProvider = createStreamsUsing(
				Component::getChildren).usingHasChildren(isLeaf);
		dump("leafs dfs", streamProvider.dsfStream(tree).filter(isLeaf));
		dump("leafs  bfs", streamProvider.bsfStream(tree).filter(isLeaf));
		dump("non-leafs dfs",
				streamProvider.dsfStream(tree).filter(isLeaf.negate()));
		dump("non-leafs  bfs",
				streamProvider.bsfStream(tree).filter(isLeaf.negate()));
	}

	private static void dump(String header, Stream<Component> stream) {
		System.out.println(header);
		System.out.println(copies('-', header.length()));
		System.out.println(stream.collect(toList()));
		System.out.println();
		System.out.println();
	}

	private static Composite c(String name, Component... children) {
		return new Composite(name, children);
	}

	private static Component l(String name) {
		return new Leaf(name);
	}

}
