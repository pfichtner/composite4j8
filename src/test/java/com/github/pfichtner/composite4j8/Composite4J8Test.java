package com.github.pfichtner.composite4j8;

import static com.github.pfichtner.composite4j8.Composite4J8.createStreamsUsing;
import static com.github.pfichtner.composite4j8.Composite4J8.NodeInfo.isLevel;
import static com.github.pfichtner.composite4j8.ExampleTree.treeOfLeafType;
import static com.github.pfichtner.composite4j8.hamcrest.IsStreamOf.isStreamOf;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;

import com.github.pfichtner.composite4j8.Composite4J8.NodeInfo;

public class Composite4J8Test {

	interface Component {
		Collection<Component> getChildren();
	}

	static class Composite implements Component {

		private final Collection<Component> children;

		public Composite(Component... children) {
			this(Arrays.asList(children));
		}

		public Composite(Collection<Component> children) {
			this.children = children;
		}

		@Override
		public Collection<Component> getChildren() {
			return children;
		}
	}

	static class LeafWithNullChildren implements Component {
		@Override
		public Collection<Component> getChildren() {
			return null;
		}
	}

	static class LeafWithEmptyCollectionChildren implements Component {
		@Override
		public Collection<Component> getChildren() {
			return Collections.emptyList();
		}
	}

	private Composite4J8<Component> sut = createStreamsUsing(Component::getChildren);

	@Test
	public void treeWithSingleLeaf() {
		LeafWithNullChildren leaf = new LeafWithNullChildren();
		assertThat(sut.dsfStream(leaf), isStreamOf(leaf));
		assertThat(sut.bsfStream(leaf), isStreamOf(leaf));
	}

	@Test
	public void dsfWithNullChildren() {
		ExampleTree tree = treeOfLeafType(LeafWithNullChildren::new);
		assertThat(sut.dsfStream(tree.getRoot()), isStreamOf(tree.inDsfOrder()));
	}

	@Test
	public void bsfWithNullChildren() {
		ExampleTree tree = treeOfLeafType(LeafWithNullChildren::new);
		assertThat(sut.bsfStream(tree.getRoot()), isStreamOf(tree.inBsfOrder()));
	}

	@Test
	public void testEmptyCollectionNodes() {
		ExampleTree tree = treeOfLeafType(LeafWithEmptyCollectionChildren::new);
		Component root = tree.getRoot();
		assertThat(sut.dsfStream(root), isStreamOf(tree.inDsfOrder()));
		assertThat(sut.bsfStream(root), isStreamOf(tree.inBsfOrder()));
	}

	@Test
	public void canFilterOnLevel() {
		ExampleTree tree = treeOfLeafType(LeafWithEmptyCollectionChildren::new);
		Component root = tree.getRoot();
		assertThat(
				sut.dsfStream_internal(root).filter(isLevel(1))
						.map(NodeInfo::getNode),
				isStreamOf(tree.nodesOnLevel(1)));
	}

}
