package com.github.pfichtner.composite4j8;

import static java.util.Arrays.asList;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import com.github.pfichtner.composite4j8.Composite4J8Test.Component;
import com.github.pfichtner.composite4j8.Composite4J8Test.Composite;

/**
 * <pre>
 *      root
 *      / \
 *     /   \
 *    a     b
 *   / \
 *  /   \
 * a1   a2
 * </pre>
 **/
public class ExampleTree {

	private final Component root;
	private final Component a;
	private final Component a1;
	private final Component a2;
	private final Component b;

	public static ExampleTree treeOfLeafType(
			Supplier<? extends Component> leafSupplier) {
		return new ExampleTree(leafSupplier);
	}

	private ExampleTree(Supplier<? extends Component> leafSupplier) {
		root = new Composite(a = new Composite(a1 = leafSupplier.get(),
				a2 = leafSupplier.get()), b = leafSupplier.get());
	}

	public Component getRoot() {
		return root;
	}

	public List<Component> inDsfOrder() {
		return asList(root, a, a1, a2, b);
	}

	public List<Component> inBsfOrder() {
		return asList(root, a, b, a1, a2);
	}

	public List<Component> nodesOnLevel(int level) {
		if (level == 1) {
			return asList(a, b);
		}
		throw new UnsupportedOperationException("level " + level);
	}

}