package com.github.pfichtner.composite4j8.hamcrest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

public final class IsStreamOf<T> extends TypeSafeMatcher<Stream<T>> {

	private final List<T> expected;

	public IsStreamOf(List<T> expected) {
		this.expected = expected;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(String.valueOf(expected));
	}

	@Override
	protected boolean matchesSafely(Stream<T> stream) {
		return expected.equals(stream.collect(Collectors.toList()));
	}

	@SafeVarargs
	@Factory
	public static <T> TypeSafeMatcher<Stream<T>> isStreamOf(T... expected) {
		return isStreamOf(Arrays.asList(expected));
	}

	@Factory
	public static <T> TypeSafeMatcher<Stream<T>> isStreamOf(List<T> expected) {
		return new IsStreamOf<T>(expected);
	}
}