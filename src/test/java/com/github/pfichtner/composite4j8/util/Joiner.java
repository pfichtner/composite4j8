package com.github.pfichtner.composite4j8.util;

import java.util.Iterator;

public final class Joiner {

	private String join;

	private Joiner(String join) {
		this.join = join;
	}

	public static Joiner on(String join) {
		return new Joiner(join);
	}

	public String join(Iterable<?> iterable) {
		Iterator<?> iterator = iterable.iterator();
		if (!iterator.hasNext()) {
			return "";
		}

		StringBuilder sb = new StringBuilder().append(iterator.next());
		while (iterator.hasNext()) {
			sb.append(join).append(iterator.next());

		}
		return sb.toString();
	}

}
