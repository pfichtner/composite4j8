package com.github.pfichtner.composite4j8.util;

import java.util.Arrays;

public class Strings {

	public static String copies(char c, int length) {
		char[] string = new char[length];
		Arrays.fill(string, c);
		return new String(string);
	}

}
