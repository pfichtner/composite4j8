# composite4j8 [![pipeline status](https://gitlab.com/pfichtner/composite4j8/badges/master/pipeline.svg)](https://gitlab.com/pfichtner/composite4j8/commits/master)

With lambdas (introduced with Java 8) composite pattern gets very easy to use
